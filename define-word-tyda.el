;;; define-word-tyda.el --- display tyda.se translations of word at point. -*- lexical-binding: t -*-

;; Copyright (C) 2017 Anders Johansson

;; Author: Anders Johansson <mejlaandersj@gmail.com>
;; URL: https://gitlab.com/andersjohansson/define-word-tyda
;; Version: 0.1.0
;; Modified: 2024-08-04
;; Package-Requires: ((define-word "0.1") (emacs "25.1"))
;; Keywords: dictionary, convenience

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; Adds tyda.se to define-word

;;; Code:
(require 'define-word)

(add-to-list 'define-word-services
             '(tyda "https://tyda.se/search/%s" define-word-tyda-parse)
             t)

(defun define-word-tyda-parse ()
  "Parse output from tyda site and return formatted list."
  (save-match-data
    (let (results)
      (while (re-search-forward "<ul class=\"list list-translations\"" nil t)
        (let ((limit (save-excursion (re-search-forward "</ul>" nil t)))
              inner)
          (while (re-search-forward "<li class=\"item text\">\\(?:.\\|\n\\)*?<a href=\"[^\"]+\">\\([^<]+\\)</a>" limit t)
            (push (match-string 1) inner))
          (push (mapconcat 'identity (nreverse inner) " | ") results)))
      (decode-coding-string (mapconcat 'identity (nreverse results) "\n") 'utf-8-unix t))))

(provide 'define-word-tyda)

;;; define-word-tyda.el ends here

